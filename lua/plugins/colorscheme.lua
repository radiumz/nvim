return {
  { -- Theme inspired by Atom
    "Aspectsides/base64-colors",
    priority = 1000,
  },
  -- { "nyoom-engineering/oxocarbon.nvim" },
  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "oxocarbon",
    },
  },
}
